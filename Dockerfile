# Christina Chiappini
# 2024-05-18
# Course 440 

# This directive tells us which base image we'll be using as a strating point to build our imag from. 
FROM httpd:2.4

# Defines some meta data for the image.
LABEL maintainer="christina_chiappini" email="christina.chiappini@dawsoncollege.qc.ca" modified="2024-05-18"

# Sets the working directory for our image.
WORKDIR /usr/local/apache2/htdocs/

# This directive specifies which files will be copied from the host, into the image. in this case, our files for our web app are being copied.
COPY ./app/ /usr/local/apache2/htdocs/
