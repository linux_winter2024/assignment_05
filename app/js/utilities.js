/**
 * This script contains all utility functions used for various little parts
 * of the application such as validation
 * @author Adriano D'Alonzo 2035770
 * @author Christina Chiappini 2042557
 * @version October 11, 2022
 */
"use strict";

/**
 * if all boolean values (which represent the validity) are all true,
 * it enables the add button. Makes the button disabled otherwise.
 * Coded by: Adriano D'Alonzo
 */
function validateAll() {

    // storing add button element into a variable
    let addBtn = document.getElementById('add');

    // for each input in the validInputs array,
    for (const input of validInputs) {
        // if that input is valid,
        if (input) {
            // enable the button
            enableButton(addBtn);
        // otherwise,
        } else {
            // keep the button disabled, and break out of loop.
            disableButton(addBtn);
            break;
        }
    }

}

/**
 * evaluates if user input for the given element parameter complies with regex patterns and displays visuals to
 * user, as well as setting a corresponding index of the global 'validInputs' array to either true or false
 * Coded by: Christina Chiappini
 * @param {HTMLElement} element the input element being evaluated
 */
function validateInput(element) {

    // creating object to store valid regex patters as values of keys that correspond to inputs on the web page
    let patterns = {
        project_id: /^[a-zA-Z][a-zA-Z0-9?($_\-)]{2,9}$/,
        owner_name: /^[a-zA-Z][a-zA-Z0-9?-]{2,9}$/,
        title: /^.{3,25}$/,
        category: /\b(?!none\b)\w+/,
        status: /\b(?!none\b)\w+/,
        hours: /^[0-9.]{1,3}$/,
        rate: /^[0-9.]{1,3}$/,
        desc: /.{3,65}/
    }

    // stores id of the element passed to function
    let input_type = element.id;
    // stores an array of keys of the 'patterns' object 
    let patterns_key_array = Object.keys(patterns);
    // stores index of element in patterns key array so we use the same index for validInputs
    let index = patterns_key_array.indexOf(input_type);

    // verifies if the function parameter's value matches the regex values and acts accordingly
    if (!patterns[input_type].test(element.value)) {
        showInvalid(element);
        validInputs[index] = false;
        return
    } 

    if (input_type = 'project_id') {
        let existingProjectIds = projects_array.map((project) => {
            return project.project_id
        });
        console.log(existingProjectIds);
        // verifying if project_id input already exists in global array
        if (existingProjectIds.includes(element.value)) {
            showInvalid(element, true);
            validInputs[index] = false;
            return
        }
    } 
    showValid(element);
    validInputs[index] = true;

}

/**
 * shows that an element is valid by displaying a check mark and
 * changing background color to green. Also removes error message(s) (if any)
 * Coded by: Adriano D'Alonzo
 * @param {HTMLElement} element the element to show as valid
 */
function showValid(element) {

    // as long as error_message classed tags exists...
    if (document.querySelectorAll('#error_message').length > 0) {
        // remove error message elements from DOM
        for (let i = 0, list = document.querySelectorAll('#error_message'); i < list.length; i++) {
            list[i].remove();
        }
    }

    // make element background green and make the adjacent element a checkmark image
    element.style.backgroundColor = "#0ee30e";
    element.parentNode.nextElementSibling.children[0].src = "../images/valid.png";

}

/**
 * shows that an element is invalid by displaying an 'X', 
 * changing background color to red and showing error message;
 * Coded by: Adriano D'Alonzo
 * @param {HTMLElement} element the element to show as invalid
 */
function showInvalid(element, projectIdError = false) {

    // as long as no error messages already exist for that element, display invalid icon, color and message
    if (document.querySelector('#error_message') == null) {
        let error = document.createElement('td');
        error.setAttribute('id', 'error_message');

        // making error message span accross all space available where appropriate
        error.setAttribute('colspan', '2');
        const existingProjectIds = projects_array.map((project) => {
            return project.project_id;
        });
        if (projectIdError) {
            error.textContent = `Project Id already exists`;
        } else {
            error.textContent = `Wrong format for ${element.id}`;
        }

        // setting visuals to notify user of invalid entry
        element.parentNode.parentNode.nextElementSibling.append(error);
        element.style.backgroundColor = "#ed3232";
        element.parentNode.nextElementSibling.children[0].src = "../images/invalid.png";
    }

}

/**
 * modifes style of element to be default by changing background color to default color
 * and removing any status images (checked or no checked).
 * Coded by: Christina Chiappini
 * @param {HTMLElement} element the element to set as default
 */
function showDefault(element) {

    // change background of input back to white
    element.style.backgroundColor = "white";
    //  link validation icon space to null value so nothing is displayed
    element.parentNode.nextElementSibling.children[0].src = "";

}

/**
 * sets button to be disabled and changes background color to grey
 * Coded by: Adriano D'Alonzo
 * @param {HTMLButtonElement} button button to disable
 */
let disableButton = function (button) {

    // disabling passed button and turning it grey
    button.style.backgroundColor = "grey";
    button.disabled = true;

}

/**
 * sets button to be enabled and changes background color to blue
 * Coded by: Adriano D'Alonzo
 * @param {HTMLButtonElement} button button to enable
 */
let enableButton = function (button) {

    // making button color vibrant and enabling it
    button.style.backgroundColor = "#0561ae";
    button.disabled = false;

}

/**
 * creates a table based on an objectsArray and appends that array to the parent
 * and puts array's objects into individual rows. Removes existing rows from table
 * Coded by: Adriano D'Alonzo
 * @param {Array} objectsArray array of objects to create table from
 */
function createTableFromArrayObjects(objectsArray) {

    // storing the element we want to append the object array to, in a variable
    let projectTable = document.getElementById('project_body');

    // removing all the objects that exist in the table being displayed
    projectTable.innerHTML = "";

    // rendering the search bar visible, since a project was created and searching is possible
    document.getElementById('search').style.display = "block";

    // adding row element to element with 'project-body' id
    addRowToTable(objectsArray, projectTable);
}

/**
 * creates an element, appends it to the parent, and adds text content of the element using content.
 * Coded by: Adriano D'Alonzo
 * @param {HTMLElement} element element to create
 * @param {String} content text content that the element may have
 * @param {HTMLElement} parent where to append the element
 * @returns newly formed tag
 */
function createHTMLElement(element, content, parent) {

    // creating html element, appends it to specified parent and returning it
    let newTag = document.createElement(element);
    newTag.innerHTML = content;
    parent.append(newTag);
    return newTag;

}

/**
 * appends a project to project table in DOM
 * Coded by: Christina Chiappini
 */
function appendTable() {

    // enabling write to local storage button
    enableButton(document.getElementById('writeBtn'));

    // creating project object from user inputs, pushing it to global array and creating DOM table with it
    let newProj = createProjectObject();
    projects_array.push(newProj);
    createTableFromArrayObjects(projects_array);

    // disabling add button
    disableButton(document.getElementById('add'));

    // resetting all fields after a row is added
    resetAll();

    // printing informative message to status bar
    statusBar.textContent = "Row added to table.";

}