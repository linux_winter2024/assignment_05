/**
 * This script runs all events and runs the whole application.
 * @author Adriano D'Alonzo 2035770
 * @author Christina Chiappini 1234567
 * @version October 11, 2022
 */
"use strict";

// loading DOM content
window.addEventListener('DOMContentLoaded', handlers);

// declaring global array to hold individual projects.
let projects_array = [];

// declaring global array to hold current status of inputs based on if they are valid
let validInputs = [false,false,false,false,false,false,false,false];

// declaring global variable for status bar of the table
let statusBar;

/**
 * creates variables for various elements on the page and
 * handles all events for those elements
 * Coded by: Christina Chiappini
 */
function handlers() {

    // declaring and initializing variables to store user inputs from DOM elements
    let projectId = document.getElementById('project_id');
    let ownerName = document.getElementById('owner_name');
    let title = document.getElementById('title');
    let category = document.getElementById('category');
    let hours = document.getElementById('hours');
    let rate = document.getElementById('rate');
    let status = document.getElementById('status');
    let shortDesc = document.getElementById('desc');

    // declaring and initializing variables to store DOM elements
    let tbody = document.getElementById('project_body');
    let addBtn = document.getElementById('add');
    let resetBtn = document.getElementById('reset');
    let writeBtn = document.getElementById('writeBtn');
    let appendBtn = document.getElementById('appendBtn');
    let clearBtn = document.getElementById('clearBtn');
    let loadBtn = document.getElementById('loadBtn');
    let searchBar = document.getElementById('search');
    statusBar = document.getElementById('search-results-tab');

    // declaring and initializing variables to store headings for the projects table
    let projectIdHeading = document.getElementById('projectIdHeading');
    let ownerHeading = document.getElementById('ownerHeading');
    let titleHeading = document.getElementById('titleHeading');
    let categoryHeading = document.getElementById('categoryHeading');
    let statusHeading = document.getElementById('statusHeading');
    let hoursHeading = document.getElementById('hoursHeading');
    let rateHeading = document.getElementById('rateHeading');
    let descHeading = document.getElementById('descHeading');

    // putting string-valued headings in an array which is used for sorting text
    let stringHeadings = [projectIdHeading,ownerHeading,titleHeading,categoryHeading,statusHeading,descHeading];
    // putting string-valued object keys in an array which is used for sorting text
    let stringSortingInputs = ['project_id','owner_name','title','category','status','short_desc'];

    // putting number-valued headings in an array which is used for sorting numbers
    let numberHeadings = [hoursHeading,rateHeading];
    // putting number-valued object keys in an array which is used for sorting numbers
    let numberSortingInputs = ['hours','rate'];
    
    // disabling buttons who's actions are currently irrelevant and clearing input boxes
    disableButton(addBtn);
    disableButton(writeBtn);
    resetAll();
    
    [projectId,ownerName,title,hours,rate,shortDesc,category,status].forEach(function(element) {
        if(element.id === 'category' || element.id === 'status') {
            element.addEventListener('blur', e => {
                validateInput(e.target);
                validateAll();
            });
        } else {
            element.addEventListener('keyup', e => {
                validateInput(e.target);
                validateAll();
            });
        }
    });  
    
    // adds event listener for each string-valued heading and calling sort function on each object key corresponding to the heading
    stringHeadings.forEach( (heading,index) => {
        heading.addEventListener('click', () => {
            sortProjectByStringAsc(stringSortingInputs[index]);
        });
    });

    // adds event listener for each number-valued heading and calling sort function on each object key corresponding to the heading
    numberHeadings.forEach( (element,index) => {
        element.addEventListener('click', () => {
            sortProjectByNumberAsc(numberSortingInputs[index]);
        });
    });

    // performing filtration by user input of search bar on code whenever key lifts in search bar
    searchBar.addEventListener('keyup', function() {filterObjects(searchBar.value)});

    // when clicked, add button appends project table with project made of user inputs
    addBtn.addEventListener('click', function () {appendTable();});

    // when clicked, reset button resets input fields to blank
    resetBtn.addEventListener('click', resetAll);

    // when clicked, write button writes the contents of project table to local storage
    writeBtn.addEventListener('click', writeLocalStorage);
  
    // when clicked, append button adds contents of the current project table to local storage
    appendBtn.addEventListener('click', appendLocalStorage);

    // when clicked, clear button deletes everything in local storage
    clearBtn.addEventListener('click', deleteLocalStorage);

    // when clicked, load button displays contents of local storage in table
    loadBtn.addEventListener('click', displayLocalStorage);
}