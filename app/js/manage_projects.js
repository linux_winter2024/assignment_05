/**
 * This script handles all procedures to manage the projects on their own.
 * @author Adriano D'Alonzo 2035770
 * @author Christina Chiappini 2042557
 * @version October 11, 2022
 */
"use strict";

/**
 * creates an object based on values inputed in the form and returns the project object
 * Coded by: Adriano D'Alonzo
 */
function createProjectObject () {
    
    let newProj = {
        project_id : document.getElementById('project_id').value,
        owner_name : document.getElementById('owner_name').value,
        title : document.getElementById('title').value,
        category : document.getElementById('category').value,
        status : document.getElementById('status').value,
        hours : document.getElementById('hours').value,
        rate : document.getElementById('rate').value,
        short_desc : document.getElementById('desc').value
    }

    // returning newly created project
    return newProj;
    
}

/**
 * deletes a row from the table (visually) using a cell from that row 
 * and modifies original array by placing all objects back in it without the deleted object (logically).
 * Coded by: Christina Chiappini
 * @param {HTMLTableRowElement} cellInRow the cell which determines which row to delete based on hierarchy
 */
function deleteTableRow(cellInRow) {
    
    // storing table row number of row being deleted that corresponds to it's equivalent in the global array
    let rowIndex = cellInRow.parentNode.rowIndex - 1;

    //as long as the user confirmed they want to delete the row, proceed
    if(window.confirm(`Are you sure you wish to remove ${cellInRow.parentNode.children[0].textContent} row?`)) {
        // removing row visually from table
        cellInRow.parentNode.remove();
        // removing project from global array
        projects_array.splice(rowIndex,1);
    }
}

/**
 * makes all data in row become editable and changes source for edit image.
 * Coded by: Adriano D'Alonzo
 * @param {HTMLTableRowElement} cellInRow the cell which determines which row to edit based on hierarchy
 */
function editTableRow(cellInRow) {

    // represents heading text for edit cell
    let editHeading = document.querySelector("#editHeading");
    // represents the index of the row being edited within the table
    let rowCount = cellInRow.parentNode.rowIndex - 1;
    // for loop starting at owner (since project id a primary key which shouldn't be edited)
    for(let i = 1; i < cellInRow.parentNode.children.length - 2; i++) {
        // if each of the content within the table are non-editable
        if(cellInRow.parentNode.children[i].contentEditable == "inherit") {
            // make it editable
            cellInRow.parentNode.children[i].contentEditable = true;
            // change heading text
            editHeading.textContent = 'Save?';
            // change edit image
            cellInRow.children[0].src = '../images/save.png';
          // otherwise  
        } else {
            // make content non-editable
            cellInRow.parentNode.children[i].contentEditable = 'inherit';
            // change heading text to default
            editHeading.textContent = 'Edit';
            // change edit image back to default
            cellInRow.children[0].src = '../images/edit.png';
        }
    } 

    // create a new object with the values from newly edited content in table
    let editedObject = {
        project_id : cellInRow.parentNode.children[0].textContent,
        owner_name : cellInRow.parentNode.children[1].textContent,
        title : cellInRow.parentNode.children[2].textContent,
        category : cellInRow.parentNode.children[3].textContent,
        hours : cellInRow.parentNode.children[4].textContent,
        rate : cellInRow.parentNode.children[5].textContent,
        status : cellInRow.parentNode.children[6].textContent,
        short_desc : cellInRow.parentNode.children[7].textContent
    }

    // remove old object from array (where editing occured), and replace it with new object.
    projects_array.splice(rowCount,1,editedObject);
}

/**
 * Adds a row to the array by looping through the array of objects and appending
 * them to the table
 * Coded by: Adriano D'Alonzo
 * @param {Array} projectArray array of objects to create row from
 * @param {HTMLElement} parent where to append array of object's values to
 */
function addRowToTable(projectArray,parent) {
    
    createProjectObject();
    
    // creating a row for every index in the array
    projectArray.forEach( project => {
        let row = document.createElement('tr');

        // creating table data for every value of the project and putting it inside the row
        Object.values(project).forEach( text => {
            createHTMLElement('td',text,row);
        });

        // creating trash and edit icons in DOM
        let editCell = createHTMLElement('td',`<img src="../images/edit.png">`,row);
        editCell.classList.add('editCell');
        let trashCell = createHTMLElement('td',`<img src="../images/trash.png">`,row);
        trashCell.classList.add('trashCell');

        // when the trash icon is clicked, call function to delete corresponding table row
        trashCell.addEventListener('click', () => {
            deleteTableRow(trashCell);
        })

        // when the edit icon is clicked, call function to edit corresponding table row
        editCell.addEventListener('click', () => {
            editTableRow(editCell);
        })

        // appending row element to the table element
        parent.append(row);
    
    });

}

/**
 * displays only the project rows that posses a match to the user input in the search bar,
 * or simply the already existing project rows if there are no matches
 * Coded by: Christina Chiappini
 * @param {string} query a string of user input from search bar to match values in table of projets
 */
function filterObjects(query) {

    // storing user input of search bar, and creating array to store matched rows.
    const regex = new RegExp(query);

    // storing all the projects that posses a match within their data into a variable
    let result_set = projects_array.filter(project => Object.values(project).some( input => regex.test(input)));

    // modifying status bar based on number of rows returned from query search
    if(result_set.length == 1) {
        statusBar.textContent = "Returning 1 row based on query...";
    } else if(result_set.length > 1) {
        statusBar.textContent = `Returning ${result_set.length} rows based on query...`;
    } else {
        statusBar.textContent = "No rows returned based on query...";
    }

    // displaying result set of projects when there were matches, or just original set of projects if not
    if (result_set !== undefined) {
        createTableFromArrayObjects(result_set);
    } else {
        createTableFromArrayObjects(projects_array);
    }

}

/**
 * changes all current values of form inputs to default values (nothing or none).
 * Coded by: Adriano D'Alonzo
 */
function resetAll() {

    // declaring a node list of HTML elements of the class 'inputs'
    let inputs = document.querySelectorAll('.inputs');

    // iterating through all the input fields to set them back to blank
    for(let i = 0; i < inputs.length; i++) {

        // setting checked or unchecked icons and input element backgrounds to white
        showDefault(inputs[i]);

        if(i === 3) {
            // resetting category, being the 3rd index, to the first option which should be the default
            inputs[3].value = inputs[3].options[0].value;
        } else if(i === 6) {
            // resetting status, being the 6th index, to the first option which should be the default
            inputs[6].value = inputs[6].options[0].value;
        } else {
            // if the input field is not a drop down list, reset it's text value to empty
            inputs[i].value = "";
        }

    }

}

/**
 * creates key value pair in local storage of all projects in table as value, and sequential number key
 * Coded by: Christina Chiappini
 */
function writeLocalStorage() {

    // deleting any projects currently being stored in local storage
    deleteLocalStorage();

    // adding each project from the global array as a key 
    projects_array.forEach((item, index) => {
        let projectJson = JSON.stringify(item)
        window.localStorage.setItem(index,projectJson)});

    ////////// test at the end if the status bar gets reset if page refresh, or if
    // printing informative message to status bar
    statusBar.textContent = `${projects_array.length} project(s) saved to local storage...`;

}

/**
 * adds key value pairs to local storage of of all projects in table as value, and sequential number key
 * Coded by: Christina Chiappini
 */
function appendLocalStorage() {

    // storing the number of pairs currently in storage so sequential key numbers can be added
    let existingStoragePairs = localStorage.length;

    //setting each appending object as a JSON of the value of an automatically generated key number
    projects_array.forEach((item, index) => {
        let projectJson = JSON.stringify(item)
        window.localStorage.setItem(existingStoragePairs + index,projectJson);
    });

    // printing message to status bar
    statusBar.textContent = `${projects_array.length} record(s) appended to local storge...`;

}

/**
 * clears local storage
 * Coded by: Christina Chiappini
 */
function deleteLocalStorage() {

    // deleting all local storage
    window.localStorage.clear();

    // printing message to status bar
    statusBar.textContent = "Local storage cleared...";

}

/**
 * renders project values from pairs in local storage and displays them, erases current table
 * Coded by: Christina Chiappini
 */
function displayLocalStorage() {

    // setting projets_array to null so it is blank and able to receive new values
    projects_array = [];
    
    // retrieving values from key value pairs of local storage as JSONs
    for (const value of Object.values(window.localStorage)) {
        // converting JSONs to objects and appending each one as a value of 'projects_array'
        projects_array.push(JSON.parse(value));
    }

    // re-rendering the table from projects_array in the DOM
    createTableFromArrayObjects(projects_array);

    // printing informative message to status bar
    statusBar.textContent = `Loading ${localStorage.length} content(s) from local storage...`;

}

/** ---------------HAS SOME BUGS, BUT GENERAL IDEA WORKS---------------------
 * function that is responsible for sorting the array of projects using string values
 * in ascending order, and re-prints the array of objects to the table
 * Coded by: Adriano D'Alonzo
 * @param {*} valueToSort the key of the object in which to sort by
 */
function sortProjectByStringAsc(valueToSort) {

    // sorts each object (using string algorithm) in projects_array in ascending order by using valueToSort
    projects_array.sort((x,y) => {
        if(x[valueToSort] > y[valueToSort]) {return 1;}
        if(x[valueToSort] < y[valueToSort]) {return -1;}
        return 0;
    });

    // redraws table using newly sorted array
    createTableFromArrayObjects(projects_array);
}

/**
 * function that is responsible for sorting the array of projects using number values
 * in ascending order, and re-prints the array of objects to the table
 * Coded by: Adriano D'Alonzo
 * @param {*} valueToSort the key of the object in which to sort by
 */
function sortProjectByNumberAsc(valueToSort) {
    
    // sorts each object (using number algorithm) in projects_array in ascending order by using valueToSort
    projects_array.sort( (x,y) => {
        return parseFloat(x[valueToSort]) - parseFloat(y[valueToSort]);
    });

    // redraws table using newly sorted array
    createTableFromArrayObjects(projects_array);

    // disabling add button
    // disableButton(document.getElementById('add'));
}