# Linux2024_Assignment05


## Author Info 

### Name:
	Christina Chiappini 

### Student Number:
	2042557

### Course:
	440


## Assignment
	This assignment consists of containerizing a web application, and then deploying it using two cloud services: a Netlify container, as well as an Azure container.


## Application
	The application that this container deploys is a simple JavaScript website that allows the user to create records for projects, manage and update the information of these records, as well as search through these records via a search query that matches any of the text in the details of the record.

	
## Instructions

#### To build this containerized app deployent using this repository...

				1. Clone this repository onto your machine
				2. Navigate to the "assignment_05" directory, where the Dockerfile is
				3. Run the following command to build the image locally:

					$docker build -t <whatever_you_want_to_call_the_image> .
				
				4. To run the image as a container, run the following command:

					$docker run -dit --name <container_name_of_choice> -p <deployemt_port>:80 <image_name_previously chosen>  


#### To run this containerized app deployment using Docker hub...

				1. Run the following command for docker to search for the image locally (and not find it), to downloadand build it form Docker Hub:

					$docker build -t <whatever_you_want_to_call_the_image> .

				2. Run the following command to build the image locally:

					$docker run -dit --name <container_name_of_choice> -p <deployment_port>:80 <image_name_previously_chosen>
